//
//  main.m
//  Sample App
//
//  Created by Timothy Perfitt on 12/6/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
