//
//  ViewController.m
//  Sample App
//
//  Created by Timothy Perfitt on 12/6/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}


- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
