//
//  AppDelegate.m
//  Sample App
//
//  Created by Timothy Perfitt on 12/6/21.
//  Copyright © 2021 Twocanoes Software. All rights reserved.
//

#import "AppDelegate.h"
#import <TCSPromo/TCSPromoManager.h>

@interface AppDelegate ()


@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    [[TCSPromoManager sharedPromoManager] showPromoWindow:self];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


- (BOOL)applicationSupportsSecureRestorableState:(NSApplication *)app {
    return YES;
}


@end
