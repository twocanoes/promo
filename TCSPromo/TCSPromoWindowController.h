//
//  TCSPromotionWindowController.h
//
//  Created by Timothy Perfitt on 10/7/18.
//

#import <Cocoa/Cocoa.h>
#import "TCSPromoSoftware.h"


NS_ASSUME_NONNULL_BEGIN

@interface TCSPromoWindowController : NSWindowController <NSCollectionViewDelegate, NSCollectionViewDataSource>
@property (assign) BOOL ignoreCurrentApp;
- (instancetype)initWithSoftware:(TCSPromoSoftware *)inSoftware ;
@end

NS_ASSUME_NONNULL_END
