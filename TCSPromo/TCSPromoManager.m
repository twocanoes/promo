//
//  PromotionManager.m
//  Promotional
//
//  Created by Timothy Perfitt on 10/7/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "TCSPromoManager.h"
#import "TCSPromoSoftware.h"

@interface TCSPromoManager()
@property (strong) TCSPromoSoftware *promoSoftware;
@end
@implementation TCSPromoManager


+ (id)sharedPromoManager {
    static TCSPromoManager *promoManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        promoManager = [[TCSPromoManager alloc] init];
    });
    return promoManager;
}

- (instancetype)initWithSettings:(NSURL *)settingsURL{
    self = [super init];
    if (self) {
        self.promoSoftware=[[TCSPromoSoftware alloc] initWithSettingsURL:settingsURL];
        if (self.promoSoftware) self.promoWindowController=[[TCSPromoWindowController alloc] initWithSoftware:self.promoSoftware];
        if (!self.promoSoftware) return nil;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {

        NSBundle *mainBundle=[NSBundle mainBundle];
        if (!mainBundle) return nil;;
        NSDictionary *info=[mainBundle infoDictionary];
        if (!info) return nil;

        NSString *settingsPath=[info objectForKey:@"TCSPromoURL"];
        if (!settingsPath) {
            NSLog(@"You must define a TCSPromoURL in your Info.plist");
            return nil;
        }

        NSURL *settings=[NSURL URLWithString:settingsPath];
        self=[self initWithSettings:settings];

    }
    return self;
}

-(void)showPromoWindow:(id)sender{
    if (!self.promoWindowController) self.promoWindowController=[[TCSPromoWindowController alloc] initWithSoftware:self.promoSoftware];
        [NSApp activateIgnoringOtherApps:YES];

        [self.promoWindowController.window makeKeyAndOrderFront:self];
}


@end
