//
//  TCSPromotionWindowController.m
//  Salute
//
//  Created by Timothy Perfitt on 10/7/18.
//

#import "TCSPromoWindowController.h"
#import "TCSPromoCollectionViewItem.h"
#import "TCSPromoHeaderViewController.h"

@interface TCSPromoWindowController ()
@property (weak) IBOutlet NSCollectionView *promotionCollectionView;
@property (strong) TCSPromoSoftware *software;
@property (strong) NSString *tagline;
@property (strong) NSString *title;
@end

@implementation TCSPromoWindowController
- (instancetype)initWithSoftware:(TCSPromoSoftware *)inSoftware
{
    self = [super initWithWindowNibName:@"TCSPromoWindowController"];
    if (self) {
        _software=inSoftware;

        if (_software.title) {
            self.title=_software.title;

            NSString *appName = [NSRunningApplication currentApplication].localizedName;
            if (appName) {
                self.title=[self.title stringByReplacingOccurrencesOfString:@"${APPNAME}" withString:appName];
            }
        }
        if (_software.tagline) self.tagline=_software.tagline;


    }
    return self;
}

-(void)awakeFromNib{

    NSNib *nib=[[NSNib alloc] initWithNibNamed:@"TCSPromoCollectionViewItem" bundle:[NSBundle bundleForClass:[self class]]];
    [self.promotionCollectionView registerNib:nib forItemWithIdentifier:@"promotionalViewItem"];
    NSNib *headerNib=[[NSNib alloc] initWithNibNamed:@"TCSPromoHeaderViewController" bundle:[NSBundle bundleForClass:[self class]]];

    [self.promotionCollectionView registerNib:headerNib forSupplementaryViewOfKind:NSCollectionElementKindSectionHeader withIdentifier:@"promotionalViewHeader"];


}
- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    //

}
- (NSSize)collectionView:(NSCollectionView *)collectionView layout:(NSCollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return NSMakeSize(200, 35);
}
- (void)collectionView:(NSCollectionView *)collectionView didSelectItemsAtIndexPaths:(NSSet<NSIndexPath *> *)indexPaths{
    NSIndexPath *indexPath=[[indexPaths allObjects] firstObject];
    NSInteger sectionIndex=[indexPath indexAtPosition:0];
    NSInteger softwareIndex=[indexPath indexAtPosition:1];
    TCSPromoSection *section=[self.software.sections objectAtIndex:sectionIndex];
    SoftwareAsset *asset=[[section softwareAssets]  objectAtIndex:softwareIndex] ;

    NSURL *learnMoreLink=[asset learnMoreLink];
    [[NSWorkspace sharedWorkspace] openURL:learnMoreLink];

}
- (nonnull NSCollectionViewItem *)collectionView:(nonnull NSCollectionView *)collectionView itemForRepresentedObjectAtIndexPath:(nonnull NSIndexPath *)indexPath {

    TCSPromoCollectionViewItem *item= [collectionView makeItemWithIdentifier:@"promotionalViewItem" forIndexPath:indexPath];

    NSInteger sectionIndex=[indexPath indexAtPosition:0];
    NSInteger softwareIndex=[indexPath indexAtPosition:1];
    TCSPromoSection *section=[self.software.sections objectAtIndex:sectionIndex];
    SoftwareAsset *asset=[[section softwareAssets]  objectAtIndex:softwareIndex] ;
    item.name=[asset name];
    item.softwareDescription=asset.softwareDescription;
    item.imageURL=asset.imageURL;
    item.learnMoreTitle=asset.learnMoreTitle;
    item.learnMoreLink=asset.learnMoreLink;
    item.isFree=asset.isFree;
    return item;
}

- (NSInteger)collectionView:(nonnull NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[self.software.sections objectAtIndex:section] softwareAssets].count;
}
- (NSInteger)numberOfSectionsInCollectionView:(NSCollectionView *)collectionView{

    return self.software.sections.count;
}
- (NSView *)collectionView:(NSCollectionView *)collectionView viewForSupplementaryElementOfKind:(NSCollectionViewSupplementaryElementKind)kind atIndexPath:(NSIndexPath *)indexPath{
    TCSPromoHeaderView *item= [collectionView makeSupplementaryViewOfKind:NSCollectionElementKindSectionHeader withIdentifier:@"promotionalViewHeader" forIndexPath:indexPath];

    NSInteger sectionIndex=[indexPath indexAtPosition:0];
    TCSPromoSection *section=[self.software.sections objectAtIndex:sectionIndex];
    item.sectionTitle=section.title;

    return item;


}

@end
