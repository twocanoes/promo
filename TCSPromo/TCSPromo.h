//
//  Promotional.h
//  Promotional
//
//  Created by Timothy Perfitt on 10/7/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Promotional.
FOUNDATION_EXPORT double PromotionalVersionNumber;

//! Project version string for Promotional.
FOUNDATION_EXPORT const unsigned char PromotionalVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Promotional/PublicHeader.h>

#import <TCSPromo/TCSPromoManager.h>

#import <TCSPromo/TCSPromoSoftware.h>
