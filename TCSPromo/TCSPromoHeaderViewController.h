//
//  TCSPromoHeaderViewController.h
//  Promotional
//
//  Created by Timothy Perfitt on 10/8/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSPromoHeaderView :NSView
@property (strong) NSString *sectionTitle;

@end
@interface TCSPromoHeaderViewController : NSViewController


@end

NS_ASSUME_NONNULL_END
