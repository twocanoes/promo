//
//  TCSPromoSoftware.h
//  Promotional
//
//  Created by Timothy Perfitt on 10/8/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface SoftwareAsset : NSObject

@property (assign) BOOL isFree;
@property (strong) NSString *name;
@property (strong) NSString *bundleIdentifier;
@property (strong) NSURL *imageURL;
@property (strong) NSString *softwareDescription;
@property (strong) NSURL *learnMoreLink;
@property (strong) NSString *learnMoreTitle;

@end

@interface TCSPromoSection : NSObject
@property (strong) NSString *title;
@property (strong) NSString *tagline;

@property (strong) NSArray <SoftwareAsset *> *softwareAssets;

@end
@interface TCSPromoSoftware : NSObject
@property (strong) NSString *title;
@property (strong) NSString *tagline;
@property (strong) NSArray *sections;
@property (assign) BOOL ignoreCurrentApp;

- (instancetype)initWithSettingsURL:(NSURL *)settingsURL;

@end

NS_ASSUME_NONNULL_END
