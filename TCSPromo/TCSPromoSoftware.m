//
//  TCSPromoSoftware.m
//  Promotional
//
//  Created by Timothy Perfitt on 10/8/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import "TCSPromoSoftware.h"

@interface TCSPromoSoftware()


@end

@implementation SoftwareAsset
@end

@implementation TCSPromoSection : NSObject
@end

@implementation TCSPromoSoftware
- (instancetype)initWithSettingsURL:(NSURL *)settingsURL
{
    self = [super init];
    if (self) {


        NSString *bundleID=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
        NSDictionary *plistSoftwareSettings=[NSDictionary dictionaryWithContentsOfURL:settingsURL];
        if (!plistSoftwareSettings) return nil;
        if (![plistSoftwareSettings objectForKey:@"Title"]) return nil;

        if (![plistSoftwareSettings objectForKey:@"Sections"]) return nil;
        if (![plistSoftwareSettings objectForKey:@"Tagline"]) return nil;

        _title=[plistSoftwareSettings objectForKey:@"Title"];
        _tagline=[plistSoftwareSettings objectForKey:@"Tagline"];
        if ([plistSoftwareSettings objectForKey:@"IgnoreCurrentApp"]) {
            _ignoreCurrentApp=[[plistSoftwareSettings objectForKey:@"IgnoreCurrentApp"] boolValue];
        }
        else
        {
            _ignoreCurrentApp=NO;
        }
        NSArray *plistSection=[plistSoftwareSettings objectForKey:@"Sections"];

        self.sections=[NSArray array];
        [plistSection enumerateObjectsUsingBlock:^(NSDictionary * plistCurrentSection, NSUInteger idx, BOOL * _Nonnull stop) {

            if (!plistCurrentSection ||
                ![plistCurrentSection objectForKey:@"Title"] ||
                ![plistCurrentSection objectForKey:@"Assets"]) {

                *stop=YES;
            }
            if (*stop==NO) {
               __block TCSPromoSection *section=[[TCSPromoSection alloc] init];
               section.title=[plistCurrentSection objectForKey:@"Title"];
                NSArray *plistMainAssets=[plistCurrentSection objectForKey:@"Assets"];
                section.softwareAssets=[NSArray array];
                [plistMainAssets enumerateObjectsUsingBlock:^(NSDictionary *plistCurrentAsset, NSUInteger idx, BOOL * _Nonnull assetStop) {
                    if ([plistCurrentAsset objectForKey:@"Name"] &&
                        [plistCurrentAsset objectForKey:@"ImageURL"] &&
                        [plistCurrentAsset objectForKey:@"Description"]  &&
                        [plistCurrentAsset objectForKey:@"LearnMoreLink"]  &&
                        [plistCurrentAsset objectForKey:@"LearnMoreTitle"] )
                    {

                        SoftwareAsset *softwareAsset=[[SoftwareAsset alloc] init];
                        softwareAsset.name=plistCurrentAsset[@"Name"];
                        softwareAsset.imageURL=[NSURL URLWithString:[plistCurrentAsset objectForKey:@"ImageURL"]];
                        softwareAsset.softwareDescription=[plistCurrentAsset objectForKey:@"Description"];
                        softwareAsset.learnMoreLink=[NSURL URLWithString:[plistCurrentAsset objectForKey:@"LearnMoreLink"]];
                        softwareAsset.learnMoreTitle=[plistCurrentAsset objectForKey:@"LearnMoreTitle"];
                        if ([plistCurrentAsset objectForKey:@"IsFree"])
                            softwareAsset.isFree=[[plistCurrentAsset objectForKey:@"IsFree"] boolValue];

//BundleIdentifier/
                        if (!(self.ignoreCurrentApp==YES && [plistCurrentAsset objectForKey:@"BundleIdentifier"] && [bundleID isEqualToString:[plistCurrentAsset objectForKey:@"BundleIdentifier"]])) {


                            section.softwareAssets=[section.softwareAssets arrayByAddingObject:softwareAsset];
                        }


                    }

                }];
                self.sections=[self.sections arrayByAddingObject:section];
            }

        }];



    }
    return self;
}
-(NSString *)description{

   __block NSString *descriptionReturn=@"";;

    descriptionReturn=[descriptionReturn stringByAppendingString:[NSString stringWithFormat:@"Title: %@\n",self.title]];
    [self.sections enumerateObjectsUsingBlock:^(TCSPromoSection *currentSection, NSUInteger idx, BOOL * _Nonnull stop) {

        NSArray *sectionSoftwareAssets=currentSection.softwareAssets;
        descriptionReturn=[descriptionReturn stringByAppendingString:[NSString stringWithFormat:@"Name: %@\n",currentSection.title]];

        [sectionSoftwareAssets enumerateObjectsUsingBlock:^(SoftwareAsset *  currentAsset, NSUInteger idx, BOOL * _Nonnull stop) {
            descriptionReturn=[descriptionReturn stringByAppendingString:[NSString stringWithFormat:@"Name: %@\n",currentAsset.name]];
            descriptionReturn=[descriptionReturn stringByAppendingString:[NSString stringWithFormat:@"ImageURL: %@\n",currentAsset.imageURL]];
            descriptionReturn=[descriptionReturn stringByAppendingString:[NSString stringWithFormat:@"Software Description: %@\n",currentAsset.softwareDescription]];
            descriptionReturn=[descriptionReturn stringByAppendingString:[NSString stringWithFormat:@"Learn More Link: %@\n",currentAsset.learnMoreLink]];
            descriptionReturn=[descriptionReturn stringByAppendingString:[NSString stringWithFormat:@"Learn More Title: %@\n",currentAsset.learnMoreTitle]];


        }];
    }];

    return descriptionReturn;
}
@end
