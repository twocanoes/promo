//
//  TCSPromitionalCollectionViewItem.h
//  Salute
//
//  Created by Timothy Perfitt on 10/7/18.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface TCSPromoCollectionViewItem : NSCollectionViewItem
@property (strong) NSString *name;
@property (strong) NSURL *imageURL;
@property (strong) NSString *softwareDescription;
@property (strong) NSURL *learnMoreLink;
@property (strong) NSString *learnMoreTitle;
@property (assign) BOOL isFree;

@end

NS_ASSUME_NONNULL_END
