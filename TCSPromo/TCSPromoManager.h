//
//  PromotionManager.h
//  Promotional
//
//  Created by Timothy Perfitt on 10/7/18.
//  Copyright © 2018 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCSPromoWindowController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TCSPromoManager : NSObject
+ (id)sharedPromoManager ;
- (instancetype)initWithSettings:(NSURL *)settingsURL;
-(void)showPromoWindow:(id)sender;
- (instancetype)init;
@property (strong) TCSPromoWindowController *promoWindowController;

@property (strong) NSURL *settingsURL;

@end

NS_ASSUME_NONNULL_END
