//
//  TCSPromitionalCollectionViewItem.m
//  Salute
//
//  Created by Timothy Perfitt on 10/7/18.
//

#import "TCSPromoCollectionViewItem.h"

@interface TCSPromoCollectionViewItem ()

@end

@implementation TCSPromoCollectionViewItem

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}
- (IBAction)openURL:(NSButtonCell *)sender {
    NSURL *learnMoreLink=[[sender representedObject] learnMoreLink];
    [[NSWorkspace sharedWorkspace] openURL:learnMoreLink];
}

@end
