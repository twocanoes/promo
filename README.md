# Promo [![Carthage compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage) 

Mac promotion framework for macOS apps.

###Description
Promo is a macOS framework that makes it easy to let your current customers know about other apps that you offer, as well as apps that you think your customers will love. And with app resellers offering referral links, you can link to other developer apps to promote them and to make a bit of extra revenue. Setting it up is easy. Add the framework to your projects, configure a single key in your Info.plist, and the setup up a plist on a web server that hosts your content. You can then show the promotion window whenever you want!

###License
Promo is licensed under the MIT license.

![Alt text](https://bitbucket.org/twocanoes/promo/raw/e8721faef5e7ad2e49d43783c1d23a9eec60a6ef/readme_screenshot.png)

## Features

* Easy setup. Carthage and framework availble.
* Shows any asset, including your own apps, apps store apps, mac apps, books, whatever.
* Configuration in a plist file on the server so easy to update
* Single plist for all your apps so you can do a single config
* Generate revenue with referral codes to other apps
* Generate traffic to your own apps
* Free badge on app icon
* User opt in and out
* Access to the window to show as a sheet or as a full window

## Carthage Setup
Carthage is the recommend way to incorporate the framework into your project. Carthage builds the framework and then you include the framework in your project. If you want to update the framework, you run the carthage command and it updates just the framework. Carthage doesn't change your project file so it keeps the framework building and your project separate.

1. Add git project to your Carthage file:
git "https://tperfitt@bitbucket.org/twocanoes/promo.git" "master"
1. run "cartage update" to build
2. Drag framework from Carthage/build/Mac/TCSPromo.framework to your frameworks in your project. Deselect the copy to project option.
1. Configure build settings to copy framework into app bundle
1. set build settings for Runtime Search path to @executable_path/../Frameworks

## Manual Setup
If you don't use Carthage, you can manually build and incorporate the framework info your project.

1. Checkout source from Github and archive build
1. Drag framework into your xcode project under Frameworks
1. Configure build settings to copy framework into app bundle
1. set build settings for Runtime Search path to @executable_path/../Frameworks

### Property List

All configuration is done via a plist file located on a web server. The URL to the plist file is in a key in the Info.plist in the app with the key "TCSPromoURL". For example:
```
	<key>TCSPromoURL</key>
	<string>https://twocanoes-app-resources.s3.amazonaws.com/PromoSoftware.plist</string>
```

The Property List is organized in 2 sections
1 Header
1 Array of Software Assets

###Header Keys
Title (String): Title shown at the top of the window. Use the variable ${APPNAME} to insert the current app name so you can use the same plist for multiple apps.
Tagline (String): The text shown right under the text at the top of the window.
IgnoreCurrentApp (Boolean): Optional. Boolean if you want to not show the current app based on bundle id.
Section (Array): Array of assets defined below. The order in the window will match the order array.

### Software Assets
The Software Assets section is a dictionary with the following keys:
Name (String): The name of the app.
BundleIdentifier (String): Optional. Bundle identifier of the app. Use for filtering if IgnoreCurrentApp is set to true.
ImageURL (String): Link to image of app. Should be 512x512 px.
Description (String): Short description of the app.
LearnMoreTitle (String): Title of the link to the app page.
LearnMoreLink (String):  URL to the app page (app store, any web page).
AppFree (Boolean): Optional. Set to true to put a "free" badge over the app icon.

#####Sample Plist:

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>Title</key>
	<string>Thanks for using ${APPNAME}!</string>
	<key>Tagline</key>
	<string>Check out some other great software for Mac and iOS</string>
	<key>IgnoreCurrentApp</key>
	<true/>
	<key>Sections</key>
	<array>
		<dict>
			<key>Title</key>
			<string>More Great Software From Twocanoes</string>
			<key>Assets</key>
			<array>
				<dict>
					<key>Name</key>
					<string>Winclone</string>
					<key>BundleIdentifier</key>
					<string>com.twocanoes.Winclone7</string>
					<key>ImageURL</key>
					<string>https://twocanoes-app-resources.s3.amazonaws.com/promo/winclone/winclone7icon.png</string>
					<key>Description</key>
					<string>Winclone 7 is the complete solution for protecting your Boot Camp Windows</string>
					<key>LearnMoreLink</key>
					<string>https://twocanoes.com/products/mac/winclone/</string>
					<key>LearnMoreTitle</key>
					<string>Learn More &gt;&gt;&gt;</string>
				</dict>
				<dict>
					<key>Name</key>
					<string>SD Clone</string>
					<key>BundleIdentifier</key>
					<string>com.twocanoes.SD-Clone</string>
					<key>ImageURL</key>
					<string>https://twocanoes-app-resources.s3.amazonaws.com/promo/sdclone/sdclone_app_icon.png</string>
					<key>Description</key>
					<string>SD Clone is the ultimate tool for cloning SD Cards right on your Mac.</string>
					<key>LearnMoreLink</key>
					<string>https://twocanoes.com/products/mac/sd-clone/</string>
					<key>LearnMoreTitle</key>
					<string>Learn More &gt;&gt;&gt;</string>
				</dict>
			</array>
		</dict>
		<dict>
			<key>Title</key>
			<string>Other Great Software for the Mac</string>
			<key>Assets</key>
			<array>
				<dict>
					<key>BundleIdentifier</key>
					<string>com.example.app</string>
					<key>Name</key>
					<string>My Great App</string>
					<key>ImageURL</key>
					<string>https://example.com/image.png</string>
					<key>Description</key>
					<string>A great App</string>
					<key>LearnMoreLink</key>
					<string>https://example.com</string>
					<key>LearnMoreTitle</key>
					<string>Learn More &gt;&gt;&gt;</string>
				</dict>
			</array>
		</dict>
	</array>
</dict>
</plist>
```

###Preferences
The checkbox in the lower left corner of the window sets the "hidePromo" NSUserDefaults and can be used to determine if the window is shown or not.

## Requirements

* Runtime: macOS 10.12 or greater
* Build: Xcode 8 and 10.12 SDK or greater
* HTTPS server for plist configuration file (see [App Transport Security](http://sparkle-project.org/documentation/app-transport-security/))

## Usage
1. import the header:

```
#import <TCSPromo/TCSPromoManager.h>
```

Show the Promo Window:

```
[[TCSPromoManager sharedPromoManager] showPromoWindow:self];
```

You can also get access to the NSWindowController to show in a sheet or use as you would any NSWindowController:

```
#import <TCSPromo/TCSPromoManager.h>
#import <TCSPromo/TCSPromoWindowController.h>

[[TCSPromoManager sharedPromoManager] promoWindowController];
```

###Delay showing
You probably don't want to show the promotion window each time, and there is not automatic display built into promo. You can set it up to show once a week like this:

```
NSDate *lastTimePromoShown=[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTimePromoShown"] ;

if (lastTimePromoShown==nil) {
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
    lastTimePromoShown=[NSDate date];
}

if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"hidePromo"] boolValue]==NO && (fabs([lastTimePromoShown timeIntervalSinceNow])>60*60*24*7) ) {

    [[TCSPromoManager sharedPromoManager] showPromoWindow:self];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastTimePromoShown"];
} 
```
## Project Sponsor

[Twocanoes Software](https://twocanoes.com/?utm_source=promo-github&utm_medium=link&utm_campaign=readme-footer)
